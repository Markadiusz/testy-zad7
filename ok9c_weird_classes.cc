#include "../tri_list.h"
#include <cassert>
#include <cstdio>
#include <iostream>
#include <set>

using std::set;

class boomboomboom : public std::exception {
	const char *what() const throw() {
		return "69";
	}
};

class Bad_access {
private:
	int dupa;

public:
	Bad_access(int x)
	: dupa(x) {}
	Bad_access(const Bad_access &x) = default;
	Bad_access(Bad_access &&x) = default;
	Bad_access &operator=([[maybe_unused]] const Bad_access &x) = delete;
	Bad_access &operator=([[maybe_unused]] Bad_access &&x) {
		x();
		dupa = x.dupa;
		return *this;
	}

	int operator()() {
		if (dupa == 138)
			throw boomboomboom();
		return dupa;
	}

	void modify() {
		dupa = 41;
	}
};

int main() {
	tri_list<int, double, Bad_access> badac;

	for (auto bad : badac.range_over<Bad_access>()) {
		assert(bad() == 138);
	}

	badac.push_back<int>(69);
	badac.push_back<Bad_access>(Bad_access(69));
	*badac.range_over<Bad_access>().begin();

	badac.modify_only<Bad_access>([](Bad_access no) {
		Bad_access new_as(no() + 69);
		return new_as;
	});

	badac.begin();
	badac.end();

	for (auto bad : badac.range_over<Bad_access>()) {
		try {
			assert(bad() == 138);
		} catch (boomboomboom &exc) {
		}
	}

	badac.modify_only<Bad_access>([](Bad_access x) {
		Bad_access y(69);
		y = std::move(x);
		y.modify();
		return y;
	});

	try {
		for (auto bad : badac.range_over<Bad_access>()) {
			assert(bad() == 41);
		}
	} catch (boomboomboom &exc) {
	}

	badac.reset<Bad_access>();

	for (auto bad : badac.range_over<Bad_access>()) {
		assert(bad() == 69);
	}
}
