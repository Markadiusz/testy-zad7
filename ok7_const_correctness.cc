#include "../tri_list.h"

int main() {
    const tri_list<int, bool, double> l{false, 1, 2.0};
    const auto it_beg = l.begin();
    const auto it_end = l.end();
    *it_beg;
    it_beg == it_end;
    it_beg != it_end;
    
    l.range_over<int>();
    l.range_over<bool>();
    l.range_over<double>();

    *l.range_over<int>().begin();
    *l.range_over<bool>().begin();
    *l.range_over<double>().begin();
}
