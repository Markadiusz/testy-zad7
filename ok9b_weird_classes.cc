#include "../tri_list.h"
#include <cassert>
#include <cstdio>
#include <iostream>
#include <set>

using std::set;

class boomboomboom : public std::exception {
	const char *what() const throw() {
		return "69";
	}
};

class Bad_assignment {
private:
	int dupa;

public:
	Bad_assignment(int x)
	: dupa(x) {}
	Bad_assignment(const Bad_assignment &x) = default;
	Bad_assignment(Bad_assignment &&x) = default;
	Bad_assignment &operator=([[maybe_unused]] Bad_assignment x) {
		throw boomboomboom();
	}
	Bad_assignment &operator=([[maybe_unused]] const Bad_assignment &x) {
		throw boomboomboom();
	}
	Bad_assignment &operator=([[maybe_unused]] Bad_assignment &&x) {
		throw boomboomboom();
	}

	int operator()() {
		return dupa;
	}
};

int main() {
	tri_list<int, double, Bad_assignment> badass;
	badass.push_back<int>(69);
	badass.push_back<Bad_assignment>(Bad_assignment(69));
	badass.modify_only<Bad_assignment>([](Bad_assignment no) {
		Bad_assignment new_as(no() + 69);
		return new_as;
	});
	badass.begin();
	badass.end();
	*badass.range_over<Bad_assignment>().begin();
	for (auto bad : badass.range_over<Bad_assignment>()) {
		assert(bad() == 138);
	}
	badass.reset<Bad_assignment>();
}
